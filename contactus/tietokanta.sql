create database mydatabase;

use mydatabase;

create table feedback (
    id int primary key auto_increment,
    name varchar(100) not null,
    email varchar(100) unique not null,
    subject varchar(50) not null,
    message text,
    saved timestamp default current_timestamp
);
    